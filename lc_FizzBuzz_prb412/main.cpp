//Link to the problem: https://leetcode.com/problems/fizz-buzz/description/t

//Write a program that outputs the string representation of numbers from 1 to n.

//But for multiples of three it should output “Fizz” instead of the number
// and for the multiples of five output “Buzz”. For numbers which are multiples
// of both three and five output “FizzBuzz”.


#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
    vector<string> fizzBuzz(int n) {
        vector<string> myVector;
        for(int i = 1; i <= n; i++){
            if(i % 5 == 0 && i % 3 ==0){
                myVector.push_back("FizzBuzz");
            }
            else if(i % 5 == 0){
                myVector.push_back("Buzz");
            }
            else if(i % 3 == 0){
                myVector.push_back("Fizz");
            }
            else{
                myVector.push_back(to_string(i));
            }

        }
        return myVector;
    }
};

int main() {
    Solution mySolution;
    vector<string> myVector = mySolution.fizzBuzz(15);
    for(int i = 0; i < myVector.size(); i++)
    std::cout << myVector[i]<< std::endl;
    return 0;
}